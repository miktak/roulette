package roulette;
import java.util.Random;
public class RouletteWheel
{
    private Random rand;
    private int number;
    RouletteWheel()
    {
        rand = new Random();
        number = 0;

    }

    public void spin()
    {
        number = rand.nextInt(37);
    }

    public int getValue()
    {
        return number;
    }
}